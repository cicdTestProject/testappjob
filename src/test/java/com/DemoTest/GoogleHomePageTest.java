package com.DemoTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GoogleHomePageTest {
	
	private WebDriver driver; 
	String appURL = "http://google.com";

	@BeforeTest
	public void testSetUp() {
		System.setProperty("webdriver.chrome.driver", "D:\\Ramakrishna\\JarFiles\\chromedriver.exe");
		   driver = new ChromeDriver();
		   driver.get("http://google.com");
		   driver.manage().deleteAllCookies();
		   driver.manage().window().maximize();
	}
	
	@Test
	public void verifyGooglePageTittle() {
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Google");
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}
	
}